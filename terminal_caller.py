import os
from configFiles import Preferences
from util import isTrue


class TerminalCaller:
    def __init__(self):
        self.config = Preferences()
        if "terminal" in self.config.config:
            self.terminal = self.config.config['terminal']
            return
        if self.isExoPressent():
            self.terminal = "exo-open --launch TerminalEmulator"
            return
        if "DESKTOP_SESSION" in os.environ:
            session = os.path.split(os.environ['DESKTOP_SESSION'])[1]
            guess = self.guessFromSession(session)
            if guess:
                self.terminal = guess
                return
        isXTERM = self.isXTERM()
        if isXTERM:
            self.terminal = isXTERM
            return
        e = "Install a terminal or configure one on "+self.config.userConfig
        raise RuntimeError(e)

    def isExoPressent(self):
        if os.path.exists("/usr/bin/exo-open"):
            return True
        return False

    def guessFromSession(self, session):
        gnome_terminal = "gnome-terminal --"
        self.consoles = {
            "plasma": "konsole -e",
            "xfce": "xfce4-terminal",
            "gnome": gnome_terminal,
            "gnome-xorg": gnome_terminal,  # Gnome has many options
            "gnome-classic": gnome_terminal,
            "budgie-desktop": gnome_terminal,  # As it's based on Gnome
            "LXDE": "lxterminal -e"
        }
        if session not in self.consoles:
            return False
        console = "/usr/bin/"+self.consoles[session]
        if os.path.exists(console.split(" ")[0]):
            return console
        return False

    def isXTERM(self):
        xterms = ["uxterm -e", "xterm -e"]
        for xterm in xterms:
            path = "/usr/bin/"+xterm
            if os.path.exists(path.split(" ")[0]):
                return path
        return False

    def get_terminal(self):
        return self.terminal

    def start(self, cmd):
        sep = ";"
        if 'autoclose' in self.config.config:
            if isTrue(self.config.config['autoclose']):
                sep = "||"
        addon = '{0}echo "Press return to exit"&&read'.format(sep)
        cmd2 = "{0}{1}".format(cmd, addon)
        end = "{0} sh -c '{1}'".format(self.terminal, cmd2)
        return os.system(end)
