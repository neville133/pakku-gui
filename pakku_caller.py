class pakku_caller:
    actions = {
        "Install": "-S",
        "Remove": "-Rusn",
        "Sysupgrade": "-Syu",
        "Remove unused": "-Rusn $(pacman -Qdtq)"
    }
    compActions = {
        "Manteniance": ["Remove unused", "Sysupgrade"]
    }

    def __init__(self, action, packages, arguments):
        if action in self.compActions:
            self.compAction = self.compActions[action]
        elif action in self.actions:
            self.action = self.actions[action]
        else:
            raise RuntimeError("Undefined action %s" % action)
        self.packages = packages
        self.arguments = arguments
        self.helper = "pakku"

    def string(self):
        try:
            st = ''
            for i in self.compAction:
                if len(st) > 0:
                    st += " && "
                st += self.fmt(self.actions[i])
            return st
        except AttributeError:
            return self.fmt()

    def fmt(self, action=None):
        if action is not None:
            self.action = action
        st = "{i.helper} {i.action} {i.packages} {i.arguments}".format(i=self)
        return st
