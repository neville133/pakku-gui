#!/usr/bin/python3

from window import MainWindow
import signal

win = MainWindow("Pakku Gui")

# Attempt to exit correctly
signal.signal(signal.SIGINT, win.gracefulShutdown)
signal.signal(signal.SIGTERM, win.gracefulShutdown)
win.setDestroyHandler(win)

win.show_all()
win.start()
