import gi
import events
import configFiles
from util import isTrue

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class MainWindow(Gtk.Window):
    def __init__(self, title="Main Window"):
        self.preferences = configFiles.Preferences()

        Gtk.Window.__init__(self, title=title)
        self.set_border_width(10)
        self.set_position(Gtk.WindowPosition.CENTER)

        hb = Gtk.HeaderBar(title=title)
        hb.set_show_close_button(True)
        self.set_titlebar(hb)

        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.add(vbox)

        stack = Gtk.Stack()
        stack.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT_RIGHT)
        stack.set_transition_duration(200)

        # Box 1 (Actions)
        box1 = self.createMainOption()
        stack.add_titled(box1, "actions", "Actions")

        # Box 2 (Preferences)
        box2 = self.createPreferences()
        stack.add_titled(box2, "preferences", "Preferences")

        stack_sw = Gtk.StackSwitcher()
        stack_sw.set_stack(stack)
        vbox.pack_start(stack, True, True, 0)
        hb.pack_start(stack_sw)

        # Save components in  the class
        self.headerBar = hb
        self.vbox = vbox
        self.stack = stack
        self.stack_sw = stack_sw

    def setDestroyHandler(self, win):
        win.connect("destroy", self.gracefulShutdown)

    def gracefulShutdown(self, *args):
        print("Exiting")
        Gtk.main_quit(args)

    def start(*args):
        return Gtk.main()

    def createMainOption(self):
        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        listbox = Gtk.ListBox()
        listbox.set_selection_mode(Gtk.SelectionMode.NONE)

        items = [
            CreateButton("Install", events.Button, True, self.preferences),
            CreateButton("Remove", events.Button, True, self.preferences)
        ]
        for item in items:
            row = Gtk.ListBoxRow()
            hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=5)
            row.add(hbox)
            hbox.pack_start(item.preRow.left, True, True, 0)
            hbox.pack_end(item.preRow.right, False, False, 0)
            listbox.add(row)
        row = Gtk.ListBoxRow()
        updatebtn = CreateButton("Sysupgrade", events.Button, False, self.preferences).button
        row.add(updatebtn)
        mantbtn = CreateButton("Manteniance", events.Button, False, self.preferences).button
        row2=Gtk.ListBoxRow()
        row2.add(mantbtn)
        listbox.add(row)
        listbox.add(row2)

        box.pack_start(listbox, True, True, 0)
        return box

    def createPreferences(self):
        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        listbox = Gtk.ListBox()
        listbox.set_selection_mode(Gtk.SelectionMode.NONE)
        actualPreferences = self.preferences
        options = [
            CreateSwitch("Close terminal automatically", "autoclose", actualPreferences),
            CreateSwitch("Bypass confirmations", "noconfirm", actualPreferences),
            CreateSwitch("Don't check AUR", "noaur", actualPreferences),
            CreateSwitch("Force update", "force", actualPreferences)
        ]
        for option in options:
            row = Gtk.ListBoxRow()
            hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)
            row.add(hbox)
            hbox.pack_start(option.preRow.left, False, False, 0)
            hbox.pack_end(option.preRow.right, False, False, 0)
            listbox.add(row)
        box.pack_start(listbox, True, True, 0)
        return box


class CreatePreRow:
    def __init__(self, item1, item2):
        self.left = item1
        self.right = item2


class CreateSwitch:
    def __init__(self, name, option, prefs):
        label = Gtk.Label(name)
        label.set_justify(Gtk.Justification.LEFT)
        switch = Gtk.Switch()
        switch.props.valign = Gtk.Align.END
        event = events.Switches(prefs, option)
        if prefs.config is not None and option in prefs.config:
            switch.set_active(isTrue(prefs.config[option]))
        switch.connect("notify::active", event.toggle)  # Event after auto-toggle
        self.preRow = CreatePreRow(label, switch)


class CreateButton:
    def __init__(self, option, callback=None, pack_entry=True, preferences=None):
        if pack_entry:
            entry = Gtk.Entry()
            evt = events.Entry(option)
        button = Gtk.Button(option)
        button.props.name = option
        if callback is not None:
            if pack_entry:
                instance = callback(option, preferences, entry)
                entry.connect("activate", evt.activate, instance)
            else:
                instance = callback(option, preferences)
            button.connect("clicked", instance.click)
        if pack_entry:
            self.preRow = CreatePreRow(entry, button)
        else:
            self.button = button
